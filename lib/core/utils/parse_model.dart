import 'package:flutter/material.dart';

extension ParseModel on Map<String, dynamic> {
  ///parse string value
  String parseString(String forKey) {
    var result = '';
    if (this.isNotEmpty && this.containsKey(forKey)) {
      result = this[forKey].toString();
      if (result.toLowerCase() == 'null') return '';
    }
    return result;
  }

  ///parse int value
  int parseInt(String forKey) {
    var result = this[forKey]?.toString() ?? '';
    if (result.toLowerCase().contains('null')) {
      return -1;
    }
    return int.tryParse(result) ?? 0;
  }

  ///parse int value
  double parseDouble(String forKey) {
    var result = this[forKey]?.toString() ?? '';
    if (result.toLowerCase().contains('null')) {
      return -1;
    }
    return double.tryParse(result) ?? 0.0;
  }

  ///parse list String
  List<String> parseListString(String forKey) {
    List<String> _result = [];
    if (containsKey(forKey) && this[forKey] is List) {
      _result = (this[forKey] as List).map((e) => e.toString()).toList();
    }
    return _result;
  }

  ///parse map value
  Map<String, dynamic> parseMap(String forKey) {
    Map<String, dynamic> _result =
        (this[forKey] as Map<String, dynamic>?) ?? {};
    return _result;
  }

  ///parse list value
  List<Map<String, dynamic>> parseList(String forKey) {
    List<Map<String, dynamic>> _result = [];
    if (containsKey(forKey) && this[forKey] is List) {
      _result = (this[forKey] as List)
          .map((e) => Map<String, dynamic>.from(e))
          .toList();
    }
    return _result;
  }

  List<Type> parseDoubleList<Type>(String forKey, Map<String, dynamic> json) {
    if (json.containsKey(forKey) && json[forKey] is List) {
      List<Type> output = List<Type>.from((json[forKey] ?? []).map((e) {
        if (e is List) {
          print("BaoVD ===> e is $e");
          return e.map((r) {
            print("BaoVD ===> r is $r");
            return Map<String, dynamic>.from(r);
          }).toList();
        }
      }));
      return output;
    }
    return [];
  }

  ///parse bool value
  bool parseBool(String forKey) {
    final strResult = (this[forKey]?.toString() ?? '').toLowerCase();
    switch (strResult) {
      case 'true':
      case '1':
        return true;
      default:
        return false;
    }
  }
}

extension ConvertDateTime on String {
  DateTime? toDateTime() {
    return DateTime.tryParse(this);
  }

  DateTime toAbsoluteDateTime() {
    return DateTime.tryParse(this) ?? DateTime.now();
  }
}

extension Timestamp on DateTime {
  int get timestamp => (millisecondsSinceEpoch / 1000).round();
}

extension Price on String {
  int toPrice() {
    // final _sPrice = replaceAll(RegExp('[\\.\\,]'), '');
    // return int.tryParse(_sPrice) ?? 0;
    final _price = double.tryParse(this) ?? 0;
    return _price.round();
  }
}

extension ColorExtension on String {
  Color? toColor() {
    var hexColor = replaceAll('#', '');
    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }
    if (hexColor.length == 8) {
      return Color(int.parse('0x$hexColor'));
    }
    return null;
  }
}

import 'package:flutter/material.dart';

const Color primary_color = color_red_500; //Color(0xff43AE3E);

// Gray
const Color color_gray_900 = Color(0xff0f1e29);
const Color color_gray_800 = Color(0xff27343e);
const Color color_gray_700 = Color(0xff3f4b53);
const Color color_gray_600 = Color(0xff576169);
const Color color_gray_500 = Color(0xff6f787e);
const Color color_gray_400 = Color(0xff878e94);
const Color color_gray_300 = Color(0xff9fa5a9);
const Color color_gray_200 = Color(0xffb7bbbf);
const Color color_gray_100 = Color(0xffcfd2d4);
const Color color_gray_50 = Color(0xffe7e8ea);
const Color color_gray_25 = Color(0xfff2f3f4);
const Color color_gray_10 = Color(0xfffafafa);
// Red
const Color color_red_700 = Color(0xffa61a19);
const Color color_red_600 = Color(0xffd52220);
const Color color_red_500 = Color(0xffee2624);
const Color color_red_400 = Color(0xfff1514f);
const Color color_red_300 = Color(0xfff47c7b);
const Color color_red_100 = Color(0xfffbcbcb);
const Color color_red_50 = Color(0xfffde5e4);
const Color color_red_25 = Color(0xfffef2f2);
const Color color_red_10 = Color(0xfffefafa);
// Orange
const Color color_orange_700 = Color(0xffa64b09);
const Color color_orange_600 = Color(0xffd5600c);
const Color color_orange_500 = Color(0xffee6c0e);
const Color color_orange_400 = Color(0xfff1893e);
const Color color_orange_300 = Color(0xfff4a66e);
const Color color_orange_100 = Color(0xfffbdcc5);
const Color color_orange_50 = Color(0xfffdede2);
const Color color_orange_25 = Color(0xfffef6f1);
// Yellow
const Color color_yellow_600 = Color(0xff997600);
const Color color_yellow_500 = Color(0xffcc9e00);
const Color color_yellow_400 = Color(0xffffc600);
const Color color_yellow_300 = Color(0xffffd133);
const Color color_yellow_200 = Color(0xffffdc66);
const Color color_yellow_50 = Color(0xfffff2c2);
const Color color_yellow_25 = Color(0xfffff8e0);
const Color color_yellow_10 = Color(0xfffffcf0);
// Green
const Color color_green_700 = Color(0xff336f48);
const Color color_green_600 = Color(0xff449460);
const Color color_green_500 = Color(0xff55ba78);
const Color color_green_400 = Color(0xff77c793);
const Color color_green_300 = Color(0xff99d5ae);
const Color color_green_100 = Color(0xffd6efdf);
const Color color_green_50 = Color(0xffeaf6ee);
const Color color_green_25 = Color(0xfff5fbf7);
// Cyan
const Color color_cyan_700 = Color(0xff0d8183);
const Color color_cyan_600 = Color(0xff11a7a9);
const Color color_cyan_500 = Color(0xff14babd);
const Color color_cyan_400 = Color(0xff43c7ca);
const Color color_cyan_300 = Color(0xff72d5d7);
const Color color_cyan_100 = Color(0xff);
const Color color_cyan_50 = Color(0xffe2f6f7);
const Color color_cyan_25 = Color(0xfff1fbfb);
// Purple
const Color color_purple_700 = Color(0xff443d83);
const Color color_purple_600 = Color(0xff584fa8);
const Color color_purple_500 = Color(0xff6258bc);
const Color color_purple_400 = Color(0xff8179c9);
const Color color_purple_300 = Color(0xffa09ad6);
const Color color_purple_100 = Color(0xffdad7ef);
const Color color_purple_50 = Color(0xffecebf7);
const Color color_purple_25 = Color(0xfff6f5fb);
// Blue
const Color color_blue_900 = Color(0xff0d2168);
const Color color_blue_800 = Color(0xff112b86);
const Color color_blue_700 = Color(0xff133096);
const Color color_blue_600 = Color(0xff4259ab);
const Color color_blue_500 = Color(0xff7182c0);
const Color color_blue_200 = Color(0xffc7cee6);
const Color color_blue_50 = Color(0xffe2e6f2);
const Color color_blue_25 = Color(0xfff1f3f9);
// Ocean Blue
const Color color_ocean_blue_400 = Color(0xff0f62fe);
const Color color_ocean_blue_300 = Color(0xff3f81fe);
const Color color_ocean_blue_200 = Color(0xff6fa0fe);
const Color color_ocean_blue_100 = Color(0xff9fc0fe);
const Color color_ocean_blue_50 = Color(0xffcfdffe);
const Color color_ocean_blue_25 = Color(0xffe7effe);
// Black
const Color color_black = Color(0xff000000);
// White
const Color color_white = Color(0xffffffff);
const Color color_transparent = Colors.transparent;
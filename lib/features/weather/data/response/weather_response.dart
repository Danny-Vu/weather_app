import 'package:weather_app/core/responses/base_list_response.dart';
import 'package:weather_app/core/utils/parse_model.dart';

class SupportToGetCityWeatherFromListResponse extends BaseListResponse<CityWeatherResponse> {
  SupportToGetCityWeatherFromListResponse({
    String cod = '',
    String message = '',
    List<CityWeatherResponse>? list,
}) : super(cod: cod, message: message, list: list);

  factory SupportToGetCityWeatherFromListResponse.fromJson(Map<String, dynamic> json) {
    List<CityWeatherResponse>? getList(Map<String, dynamic> json) {
      var list = json.parseList('list');
      if (list.isNotEmpty) {
        return list.map((e) => CityWeatherResponse.fromJson(e)).toList();
      }
      return [];
    }
    return SupportToGetCityWeatherFromListResponse(
      cod: json.parseString('cod'),
      message: json.parseString('message'),
      list: getList(json),
    );
  }
}

class CityWeatherResponse {
  final String? id;
  final String? name;
  final TempResponse? temp;
  final List<WeatherResponse>? listWeather;

  const CityWeatherResponse({
    this.id,
    this.name,
    this.temp,
    this.listWeather,
  });

  factory CityWeatherResponse.fromJson(Map<String, dynamic> json) {
    List<WeatherResponse>? getListWeather(Map<String, dynamic> json) {
      var list = json['weather'];
      if (list != null && list is List) {
        return list.map((e) => WeatherResponse.fromJson(e)).toList();
      }
    }
    return CityWeatherResponse(
      id: json.parseString('id'),
      name: json.parseString('name'),
      temp: TempResponse.fromJson(json.parseMap('main')),
      listWeather: getListWeather(json),
    );
  }
}

class TempResponse {
  final String? temp;

  const TempResponse({this.temp = ''});

  factory TempResponse.fromJson(Map<String, dynamic> json) {
    return TempResponse(
      temp: json.parseString('temp'),
    );
  }
}

class WeatherResponse {
  final String? main;
  final String? description;
  final String? icon;

  const WeatherResponse({this.main = '', this.description = '', this.icon});

  factory WeatherResponse.fromJson(Map<String, dynamic> json) {
    return WeatherResponse(
      main: json.parseString('main'),
      description: json.parseString('description'),
      icon: json.parseString('icon'),
    );
  }
}

import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:weather_app/features/weather/data/datasources/interface_weather_data_source.dart';
import 'package:weather_app/features/weather/data/response/weather_response.dart';

class WeatherLocalDatasource implements InterfaceWeatherDatasource {
  @override
  Future<SupportToGetCityWeatherFromListResponse> searchWeather(
      String search) async {
    final data = await rootBundle.loadString(
        'assets/mock_data_test/weather/weather_found_successfully.json');
    var response =
        SupportToGetCityWeatherFromListResponse.fromJson(json.decode(data));
    await Future.delayed(const Duration(seconds: 1));
    return response;
  }
}

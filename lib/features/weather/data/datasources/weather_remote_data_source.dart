import 'package:weather_app/core/api_service/api_client.dart';
import 'package:weather_app/features/weather/data/datasources/interface_weather_data_source.dart';
import 'package:weather_app/features/weather/data/response/weather_response.dart';

class WeatherRemoteDatasource implements InterfaceWeatherDatasource {
  @override
  Future<SupportToGetCityWeatherFromListResponse> searchWeather(
      String search) async {
    var queryParameters = {
      'q': search,
      'appid': "439d4b804bc8187953eb36d2a8c26a02",
    };
    final response = await OpenWeatherClient.share.get(queryParameters);
    if (response.statusCode == 200) {
      return SupportToGetCityWeatherFromListResponse.fromJson(response.data);
    } else {
      return SupportToGetCityWeatherFromListResponse(
        cod: response.statusCode.toString(),
        message: response.statusMessage.toString(),
        list: [],
      );
    }
  }
}


import 'package:weather_app/features/weather/data/response/weather_response.dart';

abstract class InterfaceWeatherDatasource {
  Future<SupportToGetCityWeatherFromListResponse> searchWeather(String search);
}

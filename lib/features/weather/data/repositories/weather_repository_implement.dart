import 'package:dartz/dartz.dart';
import 'package:weather_app/core/error/failures.dart';
import 'package:weather_app/features/weather/data/datasources/weather_remote_data_source.dart';
import 'package:weather_app/features/weather/data/response/weather_response.dart';
import 'package:weather_app/features/weather/domain/entities/weather_entity.dart';
import 'package:weather_app/features/weather/domain/repositiories/weather_repository.dart';

class WeatherRepositoryImplement extends WeatherRepository {
  final WeatherRemoteDatasource _remote = WeatherRemoteDatasource();

  @override
  Future<Either<Failure, CityWeatherEntity>> searchWeather(
      String? search) async {
    SupportToGetCityWeatherFromListResponse response;

    response = await _remote.searchWeather(search ?? '');
    if (response.cod == "200") {
      return Right(_convertResponseToEntity(response));
    } else {
      return Left(Failure(response.cod, message: response.message));
    }
  }

  CityWeatherEntity _convertResponseToEntity(
      SupportToGetCityWeatherFromListResponse response) {
    CityWeatherEntity result = const CityWeatherEntity(
      name: "Can not find any location",
      temp: TempEntity(temp: 0.0),
    );
    if (response.list?.isNotEmpty == true) {
      result = CityWeatherEntity.fromResponse(response.list!.first);
    }
    return result;
  }
}

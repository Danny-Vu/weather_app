import 'package:equatable/equatable.dart';
import 'package:weather_app/features/weather/data/response/weather_response.dart';

class CityWeatherEntity extends Equatable {
  final int id;
  final String name;
  final TempEntity temp;
  final List<WeatherEntity> listWeather;

  const CityWeatherEntity({
    this.id = -1,
    this.name = "",
    this.temp = const TempEntity(temp: 0.0),
    this.listWeather = const [],
  });

  factory CityWeatherEntity.fromResponse(CityWeatherResponse response) {
    List<WeatherEntity>? getListWeather(CityWeatherResponse response) {
      var list = response.listWeather;
      if (list?.isNotEmpty == true) {
        return list?.map((e) => WeatherEntity.fromResponse(e)).toList();
      }
      return [];
    }

    return CityWeatherEntity(
      id: int.tryParse(response.id ?? '-1') ?? -1,
      name: response.name.toString(),
      temp: TempEntity.fromResponse(
          response.temp ?? const TempResponse(temp: '')),
      listWeather: getListWeather(response) ?? [],
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        id,
        name,
        temp,
        listWeather.hashCode,
      ];
}

class TempEntity extends Equatable {
  final double temp;

  const TempEntity({this.temp = 0.0});

  factory TempEntity.fromResponse(TempResponse response) {
    return TempEntity(
      temp: double.tryParse(response.temp ?? '0.0') ?? 0.0,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [temp];
}

class WeatherEntity extends Equatable {
  final String main;
  final String description;
  final String iconUrl;

  const WeatherEntity(
      {this.main = '', this.description = '', this.iconUrl = ''});

  factory WeatherEntity.fromResponse(WeatherResponse res) {
    final url = "https://www.openweathermap.org/img/w/${res.icon}.png";
    return WeatherEntity(
      main: res.main.toString(),
      description: res.main.toString(),
      iconUrl: url,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        main,
        description,
        iconUrl,
      ];
}

import 'package:equatable/equatable.dart';
import 'package:weather_app/features/weather/domain/entities/weather_entity.dart';

abstract class WeatherState extends Equatable {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class WeatherInitialize extends WeatherState {}
class WeatherLoading extends WeatherState {}

class WeatherError extends WeatherState {
  final String? message;
  WeatherError({this.message});
}

class WeatherLoaded extends WeatherState {
  final CityWeatherEntity cityWeatherEntity;
  WeatherLoaded({required this.cityWeatherEntity});
}


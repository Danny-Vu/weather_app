import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/features/weather/domain/repositiories/weather_repository.dart';
import 'package:weather_app/features/weather/presentation/bloc/weather_event.dart';
import 'package:weather_app/features/weather/presentation/bloc/weather_state.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  final WeatherRepository _repository;

  WeatherBloc({required WeatherRepository repository})
      : _repository = repository,
        super(WeatherInitialize()) {
    on<WeatherRequest>((event, emit) async {
      emit(WeatherLoading());
      var res = await _repository
          .searchWeather((event.search != "") ? event.search : "Ho Chi Minh");
      res.fold(
        (err) => emit(WeatherError()),
        (cityWeatherEntity) => emit(WeatherLoaded(
          cityWeatherEntity: cityWeatherEntity,
        )),
      );
    },
    transformer: restartable(),
    );
  }
}

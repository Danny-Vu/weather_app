import 'package:equatable/equatable.dart';

abstract class WeatherEvent extends Equatable {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class WeatherRequest extends WeatherEvent {
  final String? search;
  WeatherRequest({this.search});
}

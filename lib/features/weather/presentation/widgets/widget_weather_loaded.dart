import 'package:flutter/material.dart';
import 'package:weather_app/core/constant/fonts.dart';
import 'package:weather_app/core/constant/spacing.dart';
import 'package:weather_app/features/weather/domain/entities/weather_entity.dart';

class WidgetWeatherLoaded extends StatelessWidget {
  final CityWeatherEntity cityWeatherEntity;
  const WidgetWeatherLoaded({super.key, required this.cityWeatherEntity});

  @override
  Widget build(BuildContext context) {
    var cityName = cityWeatherEntity.name;
    var temperature = (cityWeatherEntity.temp.temp / 10).toStringAsFixed(2);
    var description = "----";
    var iconUrl = 'https://openweathermap.org/img/w/10d.png';
    if (cityWeatherEntity.listWeather.isNotEmpty) {
       description = cityWeatherEntity.listWeather.first.description;
       iconUrl = cityWeatherEntity.listWeather.first.iconUrl;
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(cityName, style: Title_30_Bold_Invert,),
        const SizedBox(height: spacing_lg,),
        Text("$temperature ° C", style: Title_30_Bold_Invert,),
        const SizedBox(height: spacing_lg,),
        Text(description, style: Title_30_Bold_Invert,),
        const SizedBox(height: spacing_lg,),
        Image.network(iconUrl, width: spacing_4xl, height: spacing_4xl,),
      ],
    );
  }
}

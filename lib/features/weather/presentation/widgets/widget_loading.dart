import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:weather_app/core/constant/colors.dart';
import 'package:weather_app/core/constant/spacing.dart';

class WidgetLoading extends StatelessWidget {
  const WidgetLoading({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _widgetShimmer(200, 30),
        const SizedBox(height: spacing_lg,),
        _widgetShimmer(100, 25),
        const SizedBox(height: spacing_lg,),
        _widgetShimmer(100, 25),
        const SizedBox(height: spacing_lg,),
        _widgetShimmer(50, 50),
      ],
    );
  }

  Widget _widgetShimmer(double width, double height) {
    return SizedBox(
      width: width,
      height: height,
      child: Shimmer.fromColors(
        enabled: true,
        baseColor: color_gray_50,
        highlightColor: color_gray_200, child: Container(
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(spacing_sm)),
          color: Colors.white,
        ),
      ),
      ),
    );
  }
}

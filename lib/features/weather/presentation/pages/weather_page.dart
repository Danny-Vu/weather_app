import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/core/constant/colors.dart';
import 'package:weather_app/core/constant/spacing.dart';
import 'package:weather_app/features/weather/presentation/bloc/weather_bloc.dart';
import 'package:weather_app/features/weather/presentation/bloc/weather_state.dart';
import 'package:weather_app/features/weather/presentation/widgets/widget_loading.dart';
import 'package:weather_app/features/weather/presentation/widgets/widget_weather_loaded.dart';

import '../bloc/weather_event.dart';

class WeatherPage extends StatefulWidget {
  const WeatherPage({super.key});

  @override
  State<WeatherPage> createState() => _WeatherPageState();
}

class _WeatherPageState extends State<WeatherPage> {
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    context.read<WeatherBloc>().add(WeatherRequest(search: ""));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.blue,
        body: Padding(
          padding: const EdgeInsets.fromLTRB(
              spacing_lg, spacing_4xl, spacing_lg, spacing_xl),
          child: Column(
            children: [
              TextField(
                controller: _controller,
                decoration: InputDecoration(
                  hintStyle: const TextStyle(fontSize: 17, color: Colors.white),
                  hintText: ' Search city',
                  suffixIcon: AnimatedBuilder(
                    animation: _controller,
                    builder: (BuildContext context, Widget? child) {
                        if (_controller.text.isNotEmpty) {
                          return IconButton(
                            onPressed: () {
                              _controller.clear();
                              context
                                  .read<WeatherBloc>()
                                  .add(WeatherRequest(search: ""));
                            },
                            icon: const Icon(
                              Icons.clear,
                              color: Colors.white,
                            ),
                          );
                        } else {
                          return const SizedBox();
                        }
                  },),
                  focusedBorder: const OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white, width: 1.0),
                  ),
                  enabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white, width: 1.0),
                  ),
                  contentPadding: const EdgeInsets.all(20),
                ),
                onChanged: (value) {
                  context
                      .read<WeatherBloc>()
                      .add(WeatherRequest(search: value));
                },
              ),
              const SizedBox(
                height: spacing_2xl,
              ),
              BlocBuilder<WeatherBloc, WeatherState>(builder: (context, state) {
                if (state is WeatherLoading) {
                  return const WidgetLoading();
                } else if (state is WeatherLoaded) {
                  return WidgetWeatherLoaded(
                      cityWeatherEntity: state.cityWeatherEntity);
                } else if (state is WeatherError) {
                  return Center(child: Text(state.message ?? "Error"));
                }
                return const SizedBox();
              }),
            ],
          ),
        ));
  }
}

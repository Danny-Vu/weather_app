Weather App
	
	Bloc Clean Architecture
		
		Data: 
				
				Datasources: 
						
						Local : Mock Data from fake API
						
						Remote: connect to real API
				
				Response: receive data from fake api or real api and parse it from Json
				
				Repository: get data from api and put it into response
		
		Domain:
				
				Entities: parse data from response and then show it to UI
				
				Repository: like an interface and define implements for repository of data layer
		
		Presentation:
				
				Bloc: handle logic here
						
						State: we have 5 main states for getting data and show it to widgets: 
								Initiate, Loading, Error, LoadedSuccess.
						
						Event: start to request api when user searching
						
						Bloc: handle logic for every states
				
				Pages: Show Weather Page here , handle UI to show
				
				Widgets: create small widgets to show in pages
		Core:
			
			Failure: error model
			
			APIService: using dio to call api
		
		Assets:
			
			Mock data: calling fake API to get data to show UI , we don’t need to wait for BE creates api.

Config: 
    
	Using the Flutter (Channel stable, 3.7.12)
    
	Libraries:
            cupertino_icons: ^1.0.2
            // Handle model of data
            equatable: ^2.0.5
            // handle api
            dio: ^5.3.3
            // Handle logic and manage state
            flutter_bloc: ^8.1.3
            dartz: ^0.10.1
            //Handle Loading
            shimmer: ^3.0.0
    
	API:
        https://openweathermap.org/data/2.5/find?q=london&appid=439d4b804bc8187953eb36d2a8c26a02
        q= cityName
        appid=439d4b804bc8187953eb36d2a8c26a02
        You can use this link to test api via Postman

List To do:
        
		Search weather of a city by tapping their name
        
		Show city's name, temperature, description of the weather and an icon.
        
		Handle Error if there is any error 


